# SwiftNetworkMonitor

iOS Network Monitor written in Swift language Using Apple's new [NWPathMonitor](https://developer.apple.com/documentation/network/nwpathmonitor) .
Very easy to use. Once copied to your project just **one line of code** and start getting alert of network status changes.
Support only from **iOS12**.


**USAGE:**
In your Appdelegate.swift file just write like this:
        `let _ = NetworkMonitor.shared`

The above line of code will start monitor network activity , and also in line number **58,63,68** i have used my custom **AlertView** , you can use your own Toast or alert according to your need by just replacing those lines mentioned above.You can find my AlertView codes [**here**](https://gitlab.com/ios-utilities/alertview)

Also if you want to stop listening Network activity, then just call this `NetworkMonitor.shared.stop()`, If you want to start listening again just use this `NetworkMonitor.shared.start()`.
